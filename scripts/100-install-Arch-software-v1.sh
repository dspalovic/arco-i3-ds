#!/bin/bash
set -e

#software from Arch Linux repositories

# install Minimalistic document viewer - zathura
sudo pacman -S --noconfirm --needed zathura
sudo pacman -S --noconfirm --needed zathura-cb
sudo pacman -S --noconfirm --needed zathura-djvu
sudo pacman -S --noconfirm --needed zathura-pdf-poppler
sudo pacman -S --noconfirm --needed zathura-ps
echo "################################################################"
echo "#####################    zathura installed  ####################"
echo "################################################################"

# install A text browser for the World Wide Web - lynx
sudo pacman -S --noconfirm --needed lynx
echo "################################################################"
echo "#######################    lynx installed  #####################"
echo "################################################################"

# install twin-panel (commander-style) file manager (GTK2) - doublecmd-gtk
sudo pacman -S --noconfirm --needed doublecmd-gtk2
echo "################################################################"
echo "##################    doublecmd-gtk2 installed  ################"
echo "################################################################"

# install Fast, easy, and free BitTorrent client -transmission
sudo pacman -S --noconfirm --needed transmission-gtk
sudo pacman -S --noconfirm --needed transmission-cli
echo "################################################################"
echo "##################    transmission installed  ##################"
echo "################################################################"

#install Nextcloud desktop client - nextcloud-client
sudo pacman -S --noconfirm --needed nextcloud-client
echo "################################################################"
echo "#################    nextcloud-client installed  ###############"
echo "################################################################"

# install A fully integrated 3D graphics creation suite - blender
sudo pacman -S --noconfirm --needed blender
echo "################################################################"
echo "#####################    blender installed  ####################"
echo "################################################################"

# install Standalone mail and news reader from mozilla.org - thunderbird
#sudo pacman -S --noconfirm --needed thunderbird
#echo "################################################################"
#echo "###################    thunderbird installed  ##################"
#echo "################################################################"

# install Ebook management application - calibre
#sudo pacman -S --noconfirm --needed calibre
#echo "################################################################"
#echo "#####################    calibre installed  ####################"
#echo "################################################################"

# install Flexible, powerful, server-side application for playing music & Almost exact clone of ncmpc with some new features - mpd, ncmpcpp
sh Arch/install-mpd-ncmpcpp-v*.sh
echo "################################################################"
echo "#################    mpd & ncmpcpp installed  ##################"
echo "################################################################"

# install Powerful x86 virtualization for enterprise as well as home use - virtualbox
#sudo Arch/install-virtualbox-v*.sh
#echo "################################################################"
#echo "#####################    virtualbox installed  ####################"
#echo "################################################################"

echo "################################################################"
echo "##################   arch software installed  ##################"
echo "################################################################"

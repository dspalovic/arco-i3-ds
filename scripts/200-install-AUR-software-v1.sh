#!/bin/bash
#set -e

#software from AUR repositories

# install A cross platform front-end GUI of the popular youtube-dl written in wxPython - youtube-dl-gui
sh AUR/install-youtube-dl-gui-git-v*.sh
echo "################################################################"
echo "###################  youtube-dl-gui installed  #################"
echo "################################################################"

# install All-in-one voice and text chat for gamers that's free and secure - discord
sh AUR/install-discord-v*.sh
echo "################################################################"
echo "#######################  discord installed  ####################"
echo "################################################################"

# install A general purpose 3D CAD modeler - FreeCAD
sh AUR/install-freecad-v*.sh
echo "################################################################"
echo "#######################  freecad installed  ####################"
echo "################################################################"

# install An improved screenlocker based upon XCB and PAM with color configuration support - i3lock-color
sh AUR/install-i3lock-color-v*.sh
echo "################################################################"
echo "####################  i3lock-color installed  ##################"
echo "################################################################"

# install A fast and easy-to-use status bar - polybar
sh AUR/install-polybar-v*.sh
echo "################################################################"
echo "#######################  polybar installed  ####################"
echo "################################################################"

# install A very advanced and programmable command interpreter (shell) for UNIX - zsh
sh AUR/install-zsh-v*.sh
echo "################################################################"
echo "########################  zsh installed  #######################"
echo "################################################################"

echo "################################################################"
echo "##################   AUR software installed   ##################"
echo "################################################################"

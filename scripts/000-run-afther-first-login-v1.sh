#!/bin/bash
#set -e

# software from Arch Linux repositories
# https://www.archlinux.org/packages/

sh 100-install-Arch-software-v*.sh

# software from AUR (Arch User Repositories)
# https://aur.archlinux.org/packages/

sh 200-install-AUR-software-v*.sh

# electronic software from Arch Linux and AUR repositories

sh 400-install-electronic-software-v*.sh

# games from Arch Linux and AUR repositories

sh 500-install-games-v*.sh

# Final touch settings

sh 900-final-touch-v*.sh

echo '#########################################################################'
echo '###################    ALL DONE - RESTART COMPUTER    ###################'
echo '#########################################################################'

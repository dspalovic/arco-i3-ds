#!/bin/bash
set -e

# games from Arch Linux and AUR repositories

# install Fast-paced multiplayer 2D shooter game - teeworlds
sudo pacman -S --noconfirm --needed teeworlds
echo "################################################################"
echo "####################    teeworlds installed  ###################"
echo "################################################################"

# install An engine for running Transport Tycoon Deluxe - openttd
sudo pacman -S --noconfirm --needed openttd openttd-opengfx openttd-opensfx
echo "################################################################"
echo "#####################    openttd installed  ####################"
echo "################################################################"

# install Cross-platform, 3D and historically-based real-time strategy game - 0ad
#sudo pacman -S --noconfirm --needed 0ad
#echo "################################################################"
#echo "#######################    0ad installed  ######################"
#echo "################################################################"

# install A space exploration and combat game similar to Escape Velocity - endless-sky
#sh AUR/install-endless-sky-git-v*.sh
#echo "################################################################"
#echo "###################    endless-sky installed  ##################"
#echo "################################################################"

#!/bin/bash
# stop execution of the script if an error occurs
#set -e
##################################################################################################################

##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

package="arduino"

#----------------------------------------------------------------------------------

#checking if application is already installed or else install with aur helpers
if pacman -Qi $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" is already installed"
		echo "################################################################"

else

# instaliraj arduino i arduino-docs
    sudo pacman -S $package --noconfirm --needed


    echo "################################################################"
    echo "################## "$package"  installed"
    echo "################################################################"


fi


package="arduino-avr-core"

#----------------------------------------------------------------------------------

if pacman -Qi $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" is already installed"
		echo "################################################################"

else

# instaliraj arduino i arduino-docs
sudo pacman -S $package --noconfirm --needed

    echo "################################################################"
    echo "################## "$package"  installed"
    echo "################################################################"


fi


#
# package="arduino-docs"
#
# #----------------------------------------------------------------------------------
#
# if pacman -Qi $package &> /dev/null; then
#
# 		echo "################################################################"
# 		echo "################## "$package" is already installed"
# 		echo "################################################################"
#
# else
#
# # instaliraj arduino i arduino-docs
# sudo pacman -S $package --noconfirm --needed
#
#     echo "################################################################"
#     echo "################## "$package"  installed"
#     echo "################################################################"
#
#
# fi

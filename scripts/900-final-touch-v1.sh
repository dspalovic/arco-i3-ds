#!/bin/bash
set -e

#### arduino
# add user to uucp and lock group
sudo usermod -aG uucp,lock $USER

#### i3lock-color
# copie configuratin lockscript and add premission for execution
# for laptop copie lockscript_laptop, uncoment next line and coment line after that one.
#cp ./config/i3/scripts/lockscript_laptop.sh ~/.config/i3/scripts/lockscript.sh
cp ./config/i3/scripts/lockscript.sh ~/.config/i3/scripts/lockscript.sh
chmod +x ~/.config/i3/scripts/lockscript.sh

#### polybar
cp ./config/polybar/config ~/.config/polybar/config
cp ./config/polybar/launch.sh ~/.config/polybar/launch.sh
chmod +x ~/.config/polybar/launch.sh
cp ./config/polybar/scripts/weather.py ~/.config/polybar/scripts/weather.py

#### zsh
# set zsh as default, neofetch, syntax higlight, alias and change theme to af-magic
# TODO
chsh $USER -s /bin/zsh
sudo sed -i 's/ZSH_THEME=\"robbyrussell\"/ZSH_THEME=\"af-magic\"/g' ~/.zshrc
echo '
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
neofetch
' >>  ~/.zshrc
echo "
# my alias

#list
alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -la'
alias l='ls'

#fix obvious typo's
alias cd..='cd ..'
alias pdw='pwd'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#readable output
alias df='df -h'

#pacman unlock
alias unlock='sudo rm /var/lib/pacman/db.lck'

#free
alias free='free -mt'

#continue download
alias wget='wget -c'

#userlist
alias userlist='cut -d: -f1 /etc/passwd'

#merge new settings
alias merge='xrdb -merge ~/.Xresources'

# Aliases for software managment
# pacman or pm
alias pacman='sudo pacman --color auto'
alias update='sudo pacman -Syyu'

# yay as aur helper - updates everything
alias pksyua='yay -Syu --noconfirm'

#ps
alias ps='ps auxf'
alias psgrep='ps aux | grep -v grep | grep -i -e VSZ -e'

#grub update
alias update-grub='sudo grub-mkconfig -o /boot/grub/grub.cfg'


#add new fonts
alias fc='sudo fc-cache -fv'

#get fastest mirrors in your neighborhood
alias mirror='sudo reflector --protocol https --latest 50 --number 20 --sort rate --save /etc/pacman.d/mirrorlist'
alias mirrors=mirror
" >>  ~/.zshrc

#### rofi
# set theme for rofi
cp ./config/rofi/config ~/.config/rofi/config

#### termite
# configure termite
cp ./config/termite/config ~/.config/termite/config

#### vim
# pathogen, numbering, NERDTree
# TODO set font size for airline
mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree
git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline

echo '
execute pathogen#infect()
syntax on
filetype plugin indent on
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
map <C-n> :NERDTreeToggle<CR>
set laststatus=2
let g:airline_powerline_fonts = 1
set number relativenumber
set t_Co=256
' >> ~/.vimrc

echo '####################################################################'
echo '##################       run :Helptags in vim     ##################'
echo '####################################################################'

#### wallpaper
# set wallpaper for feh
mkdir ~/Pictures/wallpaper
cp ./wallpaper/wienerberger.jpg ~/Pictures/wallpaper/

#### thunar
# TODO skript for libreoffice_print
#echo '
##! /bin/bash
#libreoffice -p "$@"
#' >> ~/.config/thunar/scripts/libreoffice_print

#### lightdm
# TODO set theme and icons ...
sudo cp ./config/lightdm/lightdm-gtk-greeter.conf /etc/lightdm/lightdm-gtk-greeter.conf

#### jgmenu
# TODO

#### This one run last
#### i3wm
# configuration for i3wm
# TODO start nextcloud
cp ./config/i3/config ~/.config/i3/config

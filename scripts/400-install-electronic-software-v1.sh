#!/bin/bash
set -e

# electronic software from Arch Linux and AUR repositories

# install Arduino prototyping platform SDK - arduino
sh Arch/install-arduino-v*.sh
echo "################################################################"
echo "####################    arduino installed  #####################"
echo "################################################################"

# install Electronic schematic and printed circuit board (PCB) design tools - kicad
#sh AUR/install-kicad-git-v*.sh
#echo "################################################################"
#echo "#######################  kicad installed  ######################"
#echo "################################################################"

# install 3D modeler using the winged edge data structure - wings3d
#sudo pacman -S --noconfirm --needed wings3d
#echo "################################################################"
#echo "#####################    wings3d installed  ####################"
#echo "################################################################"

# install Powerful suite for schematic capture and printed circuit board design - eagle
#sh AUR/install-eagle-v*.sh
#echo "################################################################"
#echo "######################    eagle installed  #####################"
#echo "################################################################"

# install An electric diagram editor - qelectortech
sh AUR/install-qelectrotech*.sh
echo "################################################################"
echo "######################    qelectrotech installed  ##############"
echo "################################################################"

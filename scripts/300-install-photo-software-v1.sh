#!/bin/bash
set -e

# photo software from Arch Linux and AUR repositories

# install Darktable utility to organize and develop raw images - darktable
sh Arch/install-darktable-v*.sh
echo "################################################################"
echo "###################    darktable installed  ####################"
echo "################################################################"

# install digiKam an advanced digital photo management application - digikam
sh Arch/install-digikam-git-v*.sh
echo "################################################################"
echo "######################  digikam installed  #####################"
echo "################################################################"


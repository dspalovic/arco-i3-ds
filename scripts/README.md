# Scripts

## 000-run-afther-first-login

## 100-install-Arch-software

Packages:
* [x] zathura
* [x] lynx
* [x] doublecmd-gtk2
* [x] tramsmission
* [x] nextcloud-client
* [x] blender
* [ ] thunderbird
* [ ] calibre
* [x] mdp, ncmpcpp
* [ ] virtualbox

## 200-install-AUR-software

Packages:
* [x] zsh
* [x] discord
* [x] youtube-dl-gui
* [x] i3lock-color
* [x] polybar

## 400-install-electronic-software

Packages:
* [x] arduino
* [ ] kicad
* [ ] wings3d
* [ ] eagle
* [ ] qelectrotech

## 500-install-games

Packages:
* [x] teeworlds
* [x] openttd
* [ ] 0ad
* [ ] endless-sky

## 900-final-touch

* [x] arduino
* [x] i3lock-color
* [x] polybar
* [x] zsh
* [x] rofi
* [x] termite
* [x] vim
* [ ] thunar
* [x] wallpaper
* [ ] lightdm
* [x] i3wm
* [ ] jgmenu

### to be or not to be

* [ ] postgresql
* [ ] django

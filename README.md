# arco-i3-ds

My personal flavor of ArcoLinux based on [ArcolinuxB](https://arcolinuxb.com) i3 minimal.

![arco-i3-ds](blob/images/arco-i3-ds_screenshot.png)

## Table of Contents

- [Installation]()
- [TODO]()
- [Links]()

## Installation

> This is my choice, you may like it and you do not have to. You can use it as such, or you can use it as the starting point for your flavor. The choice of installed apps is tailored to my needs, you'll probably need other apps, change them. Thank you [Erik Dubois](https://erikdubois.be/) for the educational and inspirational ArcoLinux sites.

#### Install ArcoLinuxB minimal

This installation assumed that some version of ArcoLinux has already been installed on the computer, although it can be started from minimal image created by ArcoLinux site (https://sourceforge.net/projects/arcolinux-community-editions/files/i3/). In this case, it is necessary to remove or add the packages of your choice before the next step. Detailed video tutorial for installation of ArcoLinuxB i3 can be found on [ArcoLinuxB site](https://arcolinuxb.com/byoi-on-arcolinux-i3/).

To install the operating system, these are my steps:
1. git clone https://gitlab.com/dspalovic/arco-i3-ds
2. run script **30** from **installation-scripts** directory to create ISO image
3. burn ISO to usb and install

```
$ lsblk
# dd bs=4M if=arcolinuxb-i3ds-v19.07.6.iso of=/dev/sdc status=progress oflag=sync
```

#### Install additional packages and add settings

After restart and first login I add additional packages and set my settings:
1. git clone https://gitlab.com/dspalovic/arco-i3-ds
2. change sudo timeout to longer period (some packages needs more time to compile and install)
3. run script **000** from **scripts** directory
4. revert changes to sudo timeout
5. manual settings that can not be done by scripts

## TODO

- [ ] virtualBox
- [ ] Thunar scripts
- [ ] lightdm settings
- [ ] jgmenu ???
- [ ] postgresql
- [ ] django
- [ ] qelectrotech

## Links

### Arch Linux

- https://www.archlinux.org/
- https://wiki.archlinux.org/

### ArcoLinux

- https://arcolinux.info/
- https://arcolinux.com/
- https://arcolinuxd.com/
- https://arcolinuxb.com
- https://arcolinuxiso.com/
- https://arcolinuxforum.com/

### i3wm

- https://github.com/brunodles/i3wm-conf

#### i3lock-color

- https://github.com/PandorasFox/i3lock-color

#### Polybar

- https://github.com/jaagr/polybar/wiki/Configuration

#### Keyboard configuration

- https://wiki.archlinux.org/index.php/Xorg/Keyboard_configuration

#### lemonbar

- https://github.com/mirekys/i3-lemonbar
- https://github.com/electro7/dot_debian/tree/master/.i3/lemonbar
- https://github.com/CopperBadger/dotfiles/tree/master/dots/.i3/lemonbar-new

### mpd & ncmpcpp

- https://wiki.archlinux.org/index.php/Ncmpcpp
- https://pastebin.com/p9ctjPSP
- https://github.com/arybczak/ncmpcpp/blob/0.8.x/doc/config
- https://www.musicpd.org/doc/html/index.html
- https://wiki.archlinux.org/index.php/Music_Player_Daemon

### Vim

- https://github.com/tpope/vim-pathogen
- https://github.com/scrooloose/nerdtree
- https://github.com/vim-airline/vim-airline

### Icons, Fonts & Themes

- https://github.com/paullinuxthemer/Arrongin-GTK
- https://github.com/paullinuxthemer/Telinkrin-GTK
- https://github.com/zayronxio/Zafiro-icons
- https://github.com/ryanoasis/nerd-fonts#option-7-unofficial-arch-user-repository-aur
- https://github.com/FortAwesome/Font-Awesome
- https://fontawesome.com/cheatsheet

### jgmenu

- https://github.com/johanmalm/jgmenu

### Nextcloud desktop

- https://github.com/nextcloud/desktop
